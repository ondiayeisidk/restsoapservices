package com.covid19.m2gl.controller;


import com.covid19.m2gl.entities.Covid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController

    public class CovidRestController {
    @Autowired
    private CovidRepository petRepository;


    @GetMapping(value ="/api/covid")
    public List<Covid> findAll(){
        return  petRepository.findAll();
    }

    @GetMapping(value = "/api/covid/{id}")
    public Optional<Covid> getOne(@PathVariable Long id){
        return  petRepository.findById(id);
    }


    @PostMapping(value = "/api/covid")
    public Covid save(@RequestBody Covid p){

        p.setDate(LocalDate.now());
        return  petRepository.save(p);
    }

    @DeleteMapping(value = "/covid/{id}")
    public void delete(@PathVariable Long id){
        petRepository.deleteById(id);
    }


    @PutMapping(value = "/covid/{id}")
    public Covid update(@RequestBody Covid p, @PathVariable Long id){
        p.setId(id);
        return  petRepository.save(p);
    }


}
