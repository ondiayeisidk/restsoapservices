package com.covid19.m2gl.controller;


import com.covid19.m2gl.entities.Covid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CovidRepository  extends JpaRepository<Covid, Long> {

}

