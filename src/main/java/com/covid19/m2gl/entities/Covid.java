package com.covid19.m2gl.entities;


import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "Covid")
public class Covid  implements Serializable {
    public static  final long  serialVersionUID=1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  long id;

    @Column(name = "nbrtest")
    private String nbrTest;

    @Column(name = "casepositif")
    private String casepositif ;

    @Column(name = "casimporte")
    private String casimporte;

    @Column(name = "deces")
    private String deces;

    @Column(name = "gueris")
    private String gueris;

    @Column(name ="date")
    private LocalDate date;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNbrTest() {
        return nbrTest;
    }

    public void setNbrTest(String nbrTest) {
        this.nbrTest = nbrTest;
    }

    public String getCasepositif() {
        return casepositif;
    }

    public void setCasepositif(String casepositif) {
        this.casepositif = casepositif;
    }

    public String getCasimporte() {
        return casimporte;
    }

    public void setCasimporte(String casimporte) {
        this.casimporte = casimporte;
    }

    public String getDeces() {
        return deces;
    }

    public void setDeces(String deces) {
        this.deces = deces;
    }

    public String getGueris() {
        return gueris;
    }

    public void setGueris(String gueris) {
        this.gueris = gueris;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setId(long id) {
        this.id = id;
    }
}
