package com.covid19.m2gl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class M2glApplication {

    public static void main(String[] args) {
        SpringApplication.run(M2glApplication.class, args);
    }

}
